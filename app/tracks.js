const express = require('express');
const Track = require('../models/Track');
const Album = require('../models/Album');
const router = express.Router();

const createRouter = () => {
    // Track get
    router.get('/', (req, res) => {
        if(req.query.album) {
            Track.find({album: req.query.album}).populate({path: 'album', populate: {path: 'performer'}})
                .then(tracks => {
                    // Album.find({});
                    res.send(tracks);
                })
        } else {
            Track.find().populate('artist album')
                .then(tracks => res.send(tracks))
                .catch(() => res.sendStatus(500))
        }
    });

    router.post('/', (req, res) => {
        const trackData = req.body;

        const track = new Track(trackData);
        track.save()
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500))
    });

    return router;
};

module.exports = createRouter;
