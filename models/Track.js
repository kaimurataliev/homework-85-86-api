const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const TrackSchema = new Schema({
    number: {
        type: Number, required: true
    },
    title: {
        type: String, required: true
    },
    album: {
        type: Schema.Types.ObjectId, ref: 'Album', required: true
    },
    artist: {
        type: Schema.Types.ObjectId, ref: 'Artist', required: true
    },
    long: {
        type: String
    }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;